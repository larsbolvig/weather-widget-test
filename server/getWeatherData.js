import rp from 'request-promise';

const getWeatherData = async (city) => {
  const weatherDataEndpoint = process.env.REACT_APP_OPEN_WEATHER_MAP_API;
  const endpointKey = process.env.REACT_APP_OPEN_WEATHER_MAP_API_KEY;

  const options = {
    uri: weatherDataEndpoint,
    qs: {
      appid: endpointKey,
      q: city,
      units: 'metric'
    },
    json: true
  };

  return rp(options);

};

export default getWeatherData;