import React from 'react';
import { shallow, mount } from 'enzyme';
import WeatherWidgetForm from './WeatherWidgetForm';

describe('WeatherWidget tests', () => {
   it('Renders WeatherWidgetForm component without crashing', () => {
      shallow(<WeatherWidgetForm />);
    });

    it('Should disable button when fetching data', () => {
      const component = shallow(<WeatherWidgetForm isFetching={true} />);
      expect(component.find('.ww-search-btn').props()["disabled"]).toBe(true);
    });


    it('Should call function from props on submit', () => {
      const mockCallBack = jest.fn();
      const component = mount(<WeatherWidgetForm handleCitySearchSubmit={mockCallBack} />);

      component.find('.ww-search-btn').simulate('submit');
      expect(mockCallBack.mock.calls.length).toEqual(1);
    });


    it('Should call function from props on input change', () => {
      const mockCallBack = jest.fn();
      const component = mount(<WeatherWidgetForm handleCityInputChange={mockCallBack} />);
      const input = component.find('input');

      input.simulate("change", { target: { value: "c" }})
      input.simulate("change", { target: { value: "co" }})

      expect(mockCallBack.mock.calls.length).toEqual(2);
    });
});
