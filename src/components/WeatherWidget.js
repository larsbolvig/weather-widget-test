import React, {Fragment} from 'react';
import axios from 'axios';
import d2d from 'degrees-to-direction';

import WeatherWidgetForm from './WeatherWidgetForm';

class WeatherWidget extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      weatherData: this.props.weatherData || null,
      errorObj: this.props.error || null,
      shouldFetch: (!this.props.weatherData && !this.props.error),
      isFetching: false,
      citySearchQuery: ''
    };

    this.handleCityInputChange = this.handleCityInputChange.bind(this);
    this.handleCitySearchSubmit = this.handleCitySearchSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const {weatherData, errorObj} = this.state;

    if (weatherData !== nextProps.weatherData || errorObj !== nextProps.error) {
      this.setState({
        weatherData: nextProps.weatherData,
        errorObj: nextProps.error
      });
    }
  }

  async getWeatherData(city) {
    this.setState({
      isFetching: true
    });
    const weatherDataEnpointUrl = process.env.REACT_APP_OPEN_WEATHER_MAP_API;
    const weatherDataEnpointKey = process.env.REACT_APP_OPEN_WEATHER_MAP_API_KEY;
    const requestedCity = city || 'Copenhagen';

    let weatherData = {};
    let errorObj = null;
    try {
      weatherData = await axios.get(weatherDataEnpointUrl, {
        params: {
          appid: weatherDataEnpointKey,
          q: requestedCity,
          units: 'metric'
        }
      });
    } catch(e) {
      errorObj = {
        statusCode: e.response.status
      }
    }

    this.setState({
      weatherData: weatherData.data,
      errorObj,
      isFetching: false
    });

    window.history.replaceState('', '', `?city=${requestedCity}`);
  }

  handleCityInputChange(event) {
    this.setState({ citySearchQuery: event.target.value });
  }

  handleCitySearchSubmit(event) {
    event.preventDefault();
    this.getWeatherData(this.state.citySearchQuery);
  }

  getErrorMessagesFromStatusCode(errorObj) {
    const errorMessagesObject = {};

    switch(errorObj.statusCode) {
      case 404:
        errorMessagesObject.header = 'City not found!';
        errorMessagesObject.body = 'Weather data for the requested city was not found. Please try again.';
        break;
    }

    return errorMessagesObject;
  }

  renderFallback() {
    return (
      <div className='panel panel-warning'>
        <div className="panel-heading"><b>Something went wrong :(</b></div>
        <ul className='list-group'>
          <li className='list-group-item'>
            <p>Something went wrong when trying to load this weather widget</p><br />
            Sorry for the inconvenience.
            </li>
        </ul>
      </div> 
    );
  }

  renderWeatherWidget(weatherData, errorObj, isFetching) {
    let errorMessages = {};
    if (errorObj) {
      errorMessages = this.getErrorMessagesFromStatusCode(errorObj);
    }

   return (
    <div className={'panel panel-' + (errorObj ? 'warning' : 'info')}>
      {errorObj ? (
        <div className="panel-heading"><b>{errorMessages.header}</b></div>
      ) : (
        <div className="panel-heading">Weather in <b>{weatherData.name}</b></div>
      )}
      <ul className='list-group'>
        {errorObj ? (
          <li className='list-group-item weather-widget-error-body'>{errorMessages.body}</li>
        ) : (
          <Fragment>
            <li className='list-group-item'>Temperature: <b>{Math.round(weatherData.main.temp)}°C</b></li>
            <li className='list-group-item'>Humidity: <b>{weatherData.main.humidity}</b></li>
            <li className='list-group-item'>Wind: <b>{Math.round(weatherData.wind.speed)} m/s {weatherData.wind.deg && d2d(weatherData.wind.deg)}</b></li>
          </Fragment>
        )}
        <li className='list-group-item'>
          <WeatherWidgetForm 
            value={this.state.citySearchQuery}
            handleCityInputChange={this.handleCityInputChange}
            handleCitySearchSubmit={this.handleCitySearchSubmit}
            isFetching={isFetching} />
        </li>
      </ul>
    </div>
   );
  }



  render() {
    const { weatherData, errorObj, isFetching } = this.state;
    if (weatherData || errorObj) {
      return (
        this.renderWeatherWidget(weatherData, errorObj, isFetching)
      );
    }

    return (
      this.renderFallback()
    );
  }

}

export default WeatherWidget;