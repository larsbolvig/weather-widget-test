import path from 'path'
import fs from 'fs'

import express from 'express'
import React from 'react'
import ReactDOMServer from 'react-dom/server'


import App from '../src/App'
import getWeatherData from './getWeatherData'

const PORT = 4000
const app = express()

// Parse URL-encoded bodies (as sent by HTML forms)
app.use(express.urlencoded({ extended: true }));

// Parse JSON bodies (as sent by API clients)
app.use(express.json());

const router = express.Router()

const serverRenderer = async (req, res) => {
  const city = req.query.city || req.body.city || 'Copenhagen';

  let weatherData = {};
  let error = null;
  try {
    weatherData = await getWeatherData(city);
  } catch(e) {
    error = e;
    weatherData.city = city;
  }
  

  fs.readFile(path.resolve('./build/index.html'), 'utf8', (err, data) => {
    if (err) {
      console.error(err)
      return res.status(500).send('An error occurred')
    }

    const props = {
      weatherData,
      error
    };
    const propsStr = JSON.stringify(props);
    return res.send(
      data.replace(
        '<div id="root"></div>',
        `<div><script>window.__INITIAL_PROPS__ = ${propsStr}</script>
         <div id="root">${ReactDOMServer.renderToString(<App {...props} />)}</div></div>`
      )
    )
  })
}
router.all('/', serverRenderer)

router.use(
  express.static(path.resolve(__dirname, '..', 'build'), { maxAge: '30d' })
)

// tell the app to use the above rules
app.use(router)

app.listen(PORT, () => {
  console.log(`Weather widget running on port ${PORT}`)
})