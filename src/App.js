import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import WeatherWidget from './components/WeatherWidget';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      weatherData: this.props.weatherData || null,
      error: this.props.error || null
    }
  }

  componentDidMount() {
    const propsFromServer = window.__INITIAL_PROPS__;
    this.setState(propsFromServer);
  }

  render() {
    return (
      <div className="App">
        <div className="weather-widget-container">
          <WeatherWidget weatherData={this.state.weatherData} error={this.state.error} />
        </div>
      </div>
    );
  }
}

export default App;
