import React from 'react';
import { shallow } from 'enzyme';
import WeatherWidget from './WeatherWidget';

describe('WeatherWidget tests', () => {
   it('Renders Weather widget without crashing', () => {
      shallow(<WeatherWidget />);
    });

    it('Should render fallback when no props is given', () => {
      const component = shallow(<WeatherWidget />);

      expect(component.hasClass('panel-warning')).toEqual(true);
      expect(component.find('.panel-heading').text()).toEqual('Something went wrong :(');
    });

    it('Should render weather widget with warning when no city is found', () => {
      const component = shallow(<WeatherWidget error={{statusCode: 404}} />);

      expect(component.hasClass('panel-warning')).toEqual(true);
      expect(component.find('.panel-heading').text()).toEqual('City not found!');
    });
});