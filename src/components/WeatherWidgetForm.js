import React from 'react';

const WeatherWidgetForm = ({handleCitySearchSubmit, handleCityInputChange, value, isFetching}) => (
  <form className='form-inline' action='/' method='POST' onSubmit={handleCitySearchSubmit}>
    <div className='form-group'>
      <input type='text' name='city' className='form-control' id='city' placeholder='City' onChange={handleCityInputChange} value={value}></input>
    </div>
    <button type='submit' disabled={isFetching} className='ww-search-btn btn btn-default'>Search</button>
  </form>
);

export default WeatherWidgetForm;