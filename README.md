# Requirements

```bash
node version 10 and above
npm version 6 and above
```


# Setup

First of all you need to copy the .env file to a .env.local file and fill in the missing api key. You can find the key in the test description.
Then when standing in the root of the 'weather-widget-test' directory run the following commands to bootstrap the application.
```bash
npm install
```
And then:
```bash
npm run build
```
And then:
```bash
node server/index.js
```
And now you should be able to see the weather widget if you open up a browser and type in the address below:
```bash
http://localhost:4000/
```

# Tests

To run the tests please use the following command. 
```bash
npm run test
```
